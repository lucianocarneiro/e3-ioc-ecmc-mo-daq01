# -----------------------------------------------------------------------------
# EtherCAT IOC for MO Data Acquisition
# -----------------------------------------------------------------------------
require essioc
require ecmccfg 7.0.1
require momuda

epicsEnvSet("IOC" ,"MBL-070MO:Ctrl-ECAT-001")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")
epicsEnvSet(P, "MBL-070MO:")
epicsEnvSet(R, "RFS-DAQ-001:")
epicsEnvSet(IOCNAME, "MBL-070MO:SC-IOC-002:")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="7.0.1"),EC_RATE=500"

# ecmc
iocshLoad("$(E3_CMD_TOP)/iocsh/modaq.iocsh")

# modaq
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH1, PV=$(IOC):DS00-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH2, PV=$(IOC):DS01-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH3, PV=$(IOC):DS02-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH4, PV=$(IOC):DS03-DataAct")

################
## Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

ecmcGrepParam *ds0*

################
## Go active:
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)
iocInit()

dbl > pvs.log

